<?php

header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$data = json_decode(file_get_contents('php://input'), true);

$postdata['ai'] = $data['ai'];
$postdata['ci'] = $data['ci'];
$postdata['gi'] = $data['gi'];
$postdata['userip'] = $data['userip'];
$postdata['firstname'] = $data['firstname'];
$postdata['lastname'] = $data['lastname'];
$postdata['email'] = $data['email'];
$postdata['phone'] = $data['phone'];
$postdata['password'] = $data['password'];

$h_username =  isset(data['x-trackbox-username']) ? data['x-trackbox-username'] : 'generic';
$h_password =  isset(data['x-trackbox-password']) ? data['x-trackbox-password'] : '5QtrmS@!';

$headers[] = 'Content-Type: application/json';
$headers[] = 'Accept: */*';
$headers[] = 'Connection: keep-alive';
$headers[] = 'x-trackbox-username: ' . $h_username;
$headers[] = 'x-trackbox-password: ' . $h_password;
$headers[] = 'x-api-key: 2643889w34df345676ssdas323tgc738';


$ch = curl_init('https://platform.mangotraffic.net/api/signup/procform');
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$response = curl_exec($ch);
curl_close($ch);
echo $response;
// $response = json_decode($response, false);