export default [
  {
    name: 'Nicole C.',
    made: '$ 350',
    img: require('@/assets/img/nikole.jpg')
  },
  {
    name: 'Amir B.',
    made: '$ 627',
    img: require('@/assets/img/amir.jpg')
  },
  {
    name: 'Sarah D.',
    made: '$ 274',
    img: require('@/assets/img/sarah.jpg')
  },
  {
    name: 'Commie K.',
    made: '$ 761',
    img: require('@/assets/img/commie.jpg')
  },
  {
    name: 'Paulina T.',
    made: '$ 324',
    img: require('@/assets/img/paulina.jpg')
  },
  {
    name: 'Yoshi C.',
    made: '$ 652',
    img: require('@/assets/img/yoshi.png')
  },
  {
    name: 'Chung D.',
    made: '$ 852',
    img: require('@/assets/img/china.jpg')
  },
  {
    name: 'Madina A.',
    made: '$ 175',
    img: require('@/assets/img/madina.jpeg')
  },
];