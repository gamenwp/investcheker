import Vue from 'vue';
import App from './App.vue';
import vuetify from "./plugins/vuetify";
// import VueTelInputVuetify from "vue-tel-input-vuetify/lib";
import VueI18n from 'vue-i18n';

import messages from './locale/index';

import VueTelInput from 'vue-tel-input';
import 'vue-tel-input/dist/vue-tel-input.css';

Vue.use(VueTelInput);

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'ru',
  messages: messages
});

/*
Vue.use(VueTelInputVuetify, {
  vuetify,
});
*/
Vue.config.productionTip = false;

new Vue({
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app');
