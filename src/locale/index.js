import _en from './en';
import _ru from './ru';
import _de from './de';
import _es from './es';
const messages = {
  en: _en,
  es: _es,
  ru: _ru,
  de: _de
}

export default messages;
