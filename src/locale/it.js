export default {
  header: {
menuItemOne:'What is it ?',
menuItemTwo:'Advantages',
menuItemThree:'Transactions',
menuItemFour:'How does it work ?',
menuItemFive:'Instructions',
menuFormName:'Connect'
  },
  main:{
    whiteFirst:'Connect to the ',
    orange:'Invest Checker System',
    whiteSecond:'and discover TOP-PERFORMING industries to invest',
  },
 secondScreen:{
title:'What is Invest Checker System?',
textOne:'World’s First AI-Powered Investment Comparison Tool To Profitable Investment Through Currencies Gold, Oil, and 4,000+ Renowned Markets. The creators of the technology are legislators in the digital market and have no competitors, which simply puts them ahead of the curve.',
textTwo:'As systems become more intelligent, AI becomes more powerful, and its uses and applications reach every stock sector and industry.'
 },

advantages:{
title:'Advantages',
itemOne:'Get Higher Returns on your investments',
itemTwo:'Rates starting from 5.5% to 24.5%',
itemThree:'Compatible with 187 types of investments',
itemFour:'Scalability - investing more effort and money increases earnings',
},
recent:{
title:'Recent trades closed with Binance Smart Chain',
tableNameItemOne:'Time',
tableNameItemTwo:'Cryptocurrency',
tableNameItemThree:'Total',
tableNameItemFour:'Equivalent',
tableNameItemFive:'ROI',
button:'Connect'
},
SmartBinance:{
title:'How does Invest Checker work?',
text:'The technology is based on a new scanning infrastructure, which is optimized for ultra-fast decision making. Together with artificial intelligence, which analyzes the information field and changes in option quotes in the hundred most influential markets in the world, the algorithm allows making successful transactions in 98.3% of cases. For the rest, at Binance Smart Chain employs professional analysts with more than 10 years of experience.',
enter:'Enter the investment sum:',
get:'Get'
},
hoItWorks:{
title:'How to connect',
itemOne:'Fill out the form',
itemTwo:'Schedule a free strategy call with our team to learn your investment options',
itemThree:'We will analyze the data and information to create the right financial plan for you.',
itemFour:'We will direct you to the right broker or investment advisor and work to `give you better returns on your investments'
},
foot:{
  copyright:'@2021 All rights Reserved',
  text:'The content found on this page should not be considered as investment advice. When investing, your capital could be at risk. This page is not intended for use in jurisdictions in which the trading or investments described are prohibited and should only be used by such persons and in such ways that are legally permitted. We pass the data you provide on the online forms to relevant third parties who will contact you in relation to investment products and advice on suitable products. We do not accept any liability and make no guarantees in this regard. If you are in doubt about the suitability of this investment or do not fully understand the terms of this website or the Offering Documents, please consult a financial advisor regulated by the Financial Conduct Authority and is authorized according to the Financial Services and Markets Act 2000. Fees: There are no fees involved in using this service or website. The website offers information on a variety of investments provided by our partners.',
},
  contact: {
    title: 'Create an account',
    firstNamePlc: 'First name',
    lastNamePlc: 'Last name',
    emailPlc: 'Email',
    phonePlc: 'Telephone',
    buttonText: 'Register now'
  },
  whatcan:{
    title:'What can Invest Checker do for you?',
    fitstItemTitle:'No Fees or Charges',
    fitstItemText:'100% free comparison services to compare potential returns and to grow your investments-We do not charge when you compare for income, growth, or research',

    secondItemTitle:'Highly Recommended',
    secondItemText:'Award-winning 24/7 high-quality customer support listens to your needs and guides you to get higher income on your investments',

    thirdItemTitle:'Rapid Response',
    thirdItemText:'Our comparison system compares hundreds of investment opportunities across investment managers and brokers in less than 3 minutes, to save your valuable time',

    fouthItemTitle:'Higher ROI',
    fouthItemText:'93% of our investors have achieved a higher return on investments ranging from 5.5% to 19.5% with the help of our comparison tools and free real-time quotes',
    btn:'register now'
  },
  comp:{
    title:'Investment comparison',
    subTitle:'Our system will compare hundreds of investment opportunities across brokers and investment managers in less than 3 mins.',
    itemOne:'Commodities',
    itemTwo:'Currencies',
    itemThree:'Gold',
    itemFour:'Silver',
    itemFive:'Oil',
    itemSix:'Energy',
    itemSeven:'EIS',
    itemEight:'Foreign Exchange',
    bottomText:'Use our comparison tools to find the best investments for you.',
    btn:'register now'
  }
}
